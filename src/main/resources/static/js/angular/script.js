/**
 * Created by adam on 15.05.17.
 */

var webchatApp = angular.module('webchatApp', ['ngRoute']);


webchatApp.config(function ($routeProvider) {
   $routeProvider
       .when('/', {
           templateUrl: 'js/angular/pages/login.html',
           controller: 'loginController'
       })
       .when('/chat', {
           templateUrl: 'js/angular/pages/chat.html',
           controller: 'chatController'
       })
});

webchatApp.controller('loginController', function ($scope, $http, $rootScope, $location) {
    $scope.login = function () {
        $scope.rmiInvoker = $rootScope.rmiInvoker;

        $http({
            headers: { 'Content-Type': 'application/json'},
            method: 'POST',
            url: '/login',
            data: {
                username: $scope.username,
                invoker: $scope.rmiInvoker
            }
        }).then(function onSuccess(response) {
            console.log("RESPONSE: " + response);
            $rootScope.token = response.data.parameters.token;
            $location.path('/chat')
        }, function onFailed(response) {
            console.log("LOGIN ERROR");
            $rootScope.token = "";
        });
    }
});

webchatApp.controller('chatController', function ($scope, $rootScope, $interval, $http, msgService) {
    $scope.token = $rootScope.token;

    $scope.messages = [];

    $scope.send = function () {
        var msg = $scope.msgToSend;
        $scope.msgToSend = "";

        $http({
            method: 'POST',
            url: '/send',
            data: {
                userToken: $scope.token,
                message: msg,
                invoker: $rootScope.rmiInvoker
            }
        }).then(function onSuccess(response) {
            console.log("OK");
        }, function onFail(response) {
            console.log("NOT OK");
        })
    };

    $interval(function() {
        msgService.getMessages($scope.token, $rootScope.rmiInvoker).then(function (res) {
            if (msgService.msgList.length > 0) {
                console.log("UWAGA: " + JSON.stringify(msgService.msgList));
                for (var i = 0; i < msgService.msgList.length; i++) {
                    $scope.messages.push(msgService.msgList[i]);
                }
            }
        });
        msgService.clearMessages();
    }, 1000);
});

webchatApp.controller('navController', function ($scope, $rootScope) {
    $scope.onRadioChange = function(){
        $rootScope.rmiInvoker = $scope.invoker;
    };
});

webchatApp.service('msgService', function msg($http, $q) {
    var msgService = this;

    msgService.msgList = [];

    msgService.getMessages = function (token, invoker) {
        var defer = $q.defer();

        $http({
            method: 'GET',
            url: '/receive',
            params: {
                userToken: token,
                invoker: invoker
            }
        }).then(function onSuccess(response) {
            console.log("OK! " + response);
            var messageList = angular.fromJson(response);
            for (var i = 0; i < messageList.data.length; i++) {
                console.log(messageList.data[i].username);
                console.log(messageList.data[i].text);
                console.log(messageList.data[i].date);
                msgService.msgList.push({
                    username: messageList.data[i].username,
                    text: messageList.data[i].text,
                    date: messageList.data[i].date});
            }
            console.log(JSON.stringify(msgService.msgList));
            defer.resolve(response);
        }, function onFail(err, status) {
            defer.reject(err);
        });

        return defer.promise;
    };

    msgService.clearMessages = function () {
        msgService.msgList = [];
    };
});