package com.goralczyka.webchatclient.forms;

/**
 * Created by adam on 16.05.17.
 */
public class GetMessagesForm {
    public String userToken;
    public String invoker;

    public GetMessagesForm(final String userToken, final String invoker) {
        this.userToken = userToken;
        this.invoker = invoker;
    }
}
