package com.goralczyka.webchatclient;

import com.goralczyka.webchatclient.beans.BurlapClient;
import com.goralczyka.webchatclient.beans.HessianClient;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.builder.SpringApplicationBuilder;
import org.springframework.boot.web.support.SpringBootServletInitializer;
import org.springframework.context.annotation.Import;

@SpringBootApplication
@Import({HessianClient.class, BurlapClient.class})
public class WebchatClientApplication extends SpringBootServletInitializer{

	@Override
	protected SpringApplicationBuilder configure(SpringApplicationBuilder builder) {
		return builder.sources(WebchatClientApplication.class);
	}

	public static void main(String[] args) {
		SpringApplication.run(WebchatClientApplication.class, args);
	}
}
