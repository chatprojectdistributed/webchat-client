package com.goralczyka.webchatclient.beans;

import com.goralczyka.chat.services.ChatService;
import lombok.extern.log4j.Log4j;
import org.springframework.context.annotation.Bean;
import org.springframework.remoting.caucho.BurlapProxyFactoryBean;

/**
 * Created by adam on 14.05.17.
 */
@Log4j
public class BurlapClient {

    @Bean
    public BurlapProxyFactoryBean burlapInvoker() {
        BurlapProxyFactoryBean invoker = new BurlapProxyFactoryBean();
        invoker.setServiceUrl("http://localhost:8080/burlap");
        invoker.setServiceInterface(ChatService.class);
        return invoker;
    }
}
