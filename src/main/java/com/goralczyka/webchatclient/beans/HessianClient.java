package com.goralczyka.webchatclient.beans;

import com.goralczyka.chat.services.ChatService;
import lombok.extern.log4j.Log4j;
import org.springframework.context.annotation.Bean;
import org.springframework.remoting.caucho.HessianProxyFactoryBean;

/**
 * Created by adam on 14.05.17.
 */
@Log4j
public class HessianClient {

    @Bean
    public HessianProxyFactoryBean hessianInvoker() {
        HessianProxyFactoryBean invoker = new HessianProxyFactoryBean();
        invoker.setServiceUrl("http://localhost:8080/hessian");
        invoker.setServiceInterface(ChatService.class);
        return invoker;
    }
}
