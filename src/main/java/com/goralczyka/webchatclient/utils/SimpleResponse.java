package com.goralczyka.webchatclient.utils;

import lombok.Getter;

import java.util.HashMap;

/**
 * Created by adam on 15.05.17.
 */
public class SimpleResponse {

    // required params
    @Getter
    private final String message;

    @Getter
    private final Integer errorCode;

    // optional params
    @Getter
    private HashMap<String, String> parameters;

    private SimpleResponse(final ResponseBuilder builder) {
        this.message = builder.message;
        this.errorCode = builder.errorCode;
        this.parameters = builder.parameters;
    }

    public static class ResponseBuilder {

        // required params
        private final String message;
        private final Integer errorCode;

        // optional params
        private HashMap<String, String> parameters;

        public ResponseBuilder(final String message, final Integer errorCode) {
            this.message = message;
            this.errorCode = errorCode;
            this.parameters = new HashMap<>();
        }

        public ResponseBuilder withParameter(final String key, final String value) {
            parameters.put(key, value);
            return this;
        }

        public SimpleResponse build() {
            return new SimpleResponse(this);
        }
    }
}
