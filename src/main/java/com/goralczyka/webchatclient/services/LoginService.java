package com.goralczyka.webchatclient.services;

import com.goralczyka.chat.exceptions.UsernameAlreadyExistsException;
import com.goralczyka.webchatclient.forms.LoginForm;
import org.springframework.stereotype.Service;

/**
 * Created by adam on 16.05.17.
 */
@Service
public class LoginService extends ClientChatService {

    public String login(final LoginForm loginForm) throws UsernameAlreadyExistsException {
        return getService(loginForm.invoker).login(loginForm.username);
    }

}
