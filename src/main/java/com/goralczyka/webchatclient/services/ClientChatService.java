package com.goralczyka.webchatclient.services;

import com.goralczyka.chat.services.ChatService;
import org.springframework.beans.factory.annotation.Autowired;

/**
 * Created by adam on 16.05.17.
 */
public class ClientChatService {

    private static final String HESSIAN = "H";
    private static final String BURLAP = "W";

    @Autowired
    ChatService hessianInvoker;

    @Autowired
    ChatService burlapInvoker;


    protected ChatService getService(final String invoker) {
        if (HESSIAN.equals(invoker)) {
            return hessianInvoker;
        }
        else if (BURLAP.equals(invoker)) {
            return burlapInvoker;
        }
        else {
            // default invoker
            return hessianInvoker;
        }
    }

}
