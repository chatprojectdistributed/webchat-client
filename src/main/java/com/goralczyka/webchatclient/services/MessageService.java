package com.goralczyka.webchatclient.services;

import com.goralczyka.webchatclient.forms.GetMessagesForm;
import com.goralczyka.webchatclient.forms.SendMessageForm;
import org.springframework.stereotype.Service;

/**
 * Created by adam on 16.05.17.
 */
@Service
public class MessageService extends ClientChatService {

    public Boolean send(final SendMessageForm messageForm) {
        return getService(messageForm.invoker).send(messageForm.userToken, messageForm.message);
    }

    public String getMessages(final GetMessagesForm messagesForm) {
        return getService(messagesForm.invoker).getMessages(messagesForm.userToken);
    }
}
