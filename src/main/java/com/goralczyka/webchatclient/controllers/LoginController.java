package com.goralczyka.webchatclient.controllers;

import com.goralczyka.chat.exceptions.UsernameAlreadyExistsException;
import com.goralczyka.webchatclient.forms.LoginForm;
import com.goralczyka.webchatclient.services.LoginService;
import com.goralczyka.webchatclient.utils.SimpleResponse;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RestController;

/**
 * Created by adam on 14.05.17.
 */
@RestController
public class LoginController {

    @Autowired
    private LoginService loginService;

    @PostMapping("/login")
    public SimpleResponse login(@RequestBody LoginForm form) throws UsernameAlreadyExistsException {
        String token = loginService.login(form);

        return new SimpleResponse.ResponseBuilder("Ok", 0)
                .withParameter("token", token)
                .build();
    }
}
