package com.goralczyka.webchatclient.controllers;

import com.goralczyka.webchatclient.forms.GetMessagesForm;
import com.goralczyka.webchatclient.forms.SendMessageForm;
import com.goralczyka.webchatclient.services.MessageService;
import com.goralczyka.webchatclient.utils.SimpleResponse;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

/**
 * Created by adam on 16.05.17.
 */
@RestController
public class MessageController {

    @Autowired
    MessageService messageService;

    @GetMapping("/receive")
    public String getMessages(@RequestParam("userToken") String userToken, @RequestParam("invoker") String invoker) {
        return messageService.getMessages(new GetMessagesForm(userToken, invoker));
    }

    @PostMapping("/send")
    public SimpleResponse sendMessage(@RequestBody SendMessageForm messageForm) {
        Boolean sendStatus = messageService.send(messageForm);
        return new SimpleResponse.ResponseBuilder(sendStatus.toString(), sendStatus.equals(true) ? 0 : 1).build();
    }


}
